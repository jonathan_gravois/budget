<div class="page-header">
    <h1>RECALCULATE OPEN P.A.M. BUDGETS</h1>
    <p class="lead">All budgeted employees in budgets that are classified as "Open" will be recalculated.</p>
    <p><button class="btn btn-edr btnRecalcOpen">Proceed</button></p>
</div>

<div class="row-fluid">
    <div class="loadingDiv"></div>
    <div id="recalcOpenReport" class="span12"></div>
 </div>